# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

_pkgname=pahole
pkgname="$_pkgname"-slim
pkgver=1.25
pkgrel=1
epoch=1
pkgdesc="Pahole and other DWARF utils - without matplotlib dependency"
url="https://git.kernel.org/pub/scm/devel/pahole/pahole.git"
arch=(x86_64)
license=(GPL2)
depends=(
  libelf
  python
)
makedepends=(
  cmake
  ninja
)
provides=(libdwarves{,_emit,_reorganize}.so 'dwarves' 'pahole')
conflicts=('dwarves' 'pahole')
source=(
  https://fedorapeople.org/~acme/dwarves/dwarves-$pkgver.tar.xz
  python.diff
)
b2sums=('1926fa1fa123fc3ad0f7f063406260b1e1e2611c563fedebee4c837e491164571fdb40408421c0c4ea2fd24e89c54a7a1ea669313b6dd6d7dcfa4934e2c1336e'
        'e88fcda77c0a6aaea0d83949cabbaaaf24f6a4b2324a14e459efc202b210c31b58f7a90c75c34ffd08911514b3b6db4be4423e3f8e0fb50b6d914da5be002319')

prepare() {
  cd dwarves-$pkgver

  # https://bugs.archlinux.org/task/70013
  patch -Np1 -i ../python.diff
}

build() {
  local cmake_options=(
    -DCMAKE_INSTALL_PREFIX=/usr
    -DCMAKE_BUILD_TYPE=None
    -D__LIB=lib
  )

  cmake -S dwarves-$pkgver -B build -G Ninja "${cmake_options[@]}"
  cmake --build build
}

check() {
  cd build
  ctest --output-on-failure --stop-on-failure -j$(nproc)
}

package() {
  DESTDIR="$pkgdir" cmake --install build

  python -m compileall -d / "$pkgdir"
  python -O -m compileall -d / "$pkgdir"
}

# vim:set sw=2 sts=-1 et:
